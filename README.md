# Simple App Template

## Getting started

Simple App Template is an app built on Angular and NestJS using a Postgres database.

You need :
- [Node.js](https://nodejs.org/)
- [Docker](https://docs.docker.com/)

Built on :
- [Angular](https://angular.io)
- [NestJS](https://nestjs.com/)

App interface built with :
- [clarity.design](https://clarity.design/)
- [sweetalert2](https://sweetalert2.github.io/)

```bash
npm install -g @nestjs/cli
npm install -g @angular/cli
```

### Launch on local

Launch the postgres database :
```bash
docker run --name simple-app-postgres -e POSTGRES_PASSWORD=password -d -p 32770:5432 postgres
```

Launch the API : 
```bash
cd api/
npm install
npm start
```

You can find the API documentation on http://localhost:3000/api/v1/docs

Launch the APP : 
```bash
cd api/
npm install
npm start
```

You can now try your app on http://localhost:4200

### Launch on docker

```bash
docker-compose up --build
```

You can now try your app on http://localhost:4200

### Modify the app

- Change the database config on api/ormconfig.json
- Edit the ressources in api/ressources/models/ressources.entity.ts
- Edit the app/ressources module to match with your ressources