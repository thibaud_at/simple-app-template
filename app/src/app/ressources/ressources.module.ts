import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ClarityModule } from '@clr/angular';

import { RessourcesRoutingModule } from './ressources-routing.module';
import { RessourcesComponent } from './ressources/ressources.component';
import { RessourcesService } from './ressources/services/ressources.service';

@NgModule({
  declarations: [RessourcesComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    ClarityModule,
    RessourcesRoutingModule,
  ],
  providers: [RessourcesService]
})
export class RessourcesModule {}
