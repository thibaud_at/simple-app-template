import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Ressource } from "../models/ressource.model";
import { environment } from 'src/environments/environment';

@Injectable()
export class RessourcesService {

  basePath = `http://${environment.API_HOST}:${environment.API_PORT}/api/v1`;
  ressourcesPath = "/ressources";

  constructor(private http: HttpClient) {}

  findAll(): Observable<Ressource[]> {
    return this.http.get<Ressource[]>(
      `${this.basePath}${this.ressourcesPath}`
    );
  }

  findOne(id: number): Observable<Ressource> {
    return this.http.get<Ressource>(
      `${this.basePath}${this.ressourcesPath}/${id}`
    );
  }

  insert(ressource: Ressource): Observable<Ressource> {
    return this.http.post<Ressource>(
      `${this.basePath}${this.ressourcesPath}`,
      ressource
    );
  }

  update(id: number, ressource: Ressource): Observable<Ressource> {
    return this.http.put<Ressource>(
      `${this.basePath}${this.ressourcesPath}/${id}`,
      ressource
    );
  }

  delete(id: number): Observable<Ressource> {
    return this.http.delete<Ressource>(
      `${this.basePath}${this.ressourcesPath}/${id}`
    );
  }


}
