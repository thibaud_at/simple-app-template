import { Component, OnInit, Inject, ViewChild, ElementRef } from "@angular/core";
import { Ressource } from './models/ressource.model';
import { RessourcesService } from "./services/ressources.service";

import Swal from 'sweetalert2'

@Component({
  selector: "app-ressources",
  templateUrl: "./ressources.component.html",
  styleUrls: ["./ressources.component.css"]
})
export class RessourcesComponent implements OnInit {
  ressources: Ressource[];
  isLoaded: boolean = false;

  constructor(
    private ressourcesService: RessourcesService,
  ) {}

  ngOnInit() {
    this.findAll();
  }

  findAll(){
    this.ressourcesService.findAll().subscribe(ressources => {
      this.ressources = ressources;
      this.isLoaded = true;
    })
  }
  
  insert(){
    Swal.mixin({
      input: 'text',
      confirmButtonText: 'Next &rarr;',
      showCancelButton: true,
      progressSteps: ['1', '2', '3']
    }).queue([
      {
        title: 'Name',
      },
      'Small description',
      {
        title: 'Is it free ?',
        input: 'select',
        inputOptions: {
          true: 'Yes',
          false: 'No'
        },
      }
    ]).then((result) => {
      if (result.value) {
        this.validateInsert(result.value);
      }
    })
  }

  validateInsert(values){
    this.isLoaded = false;
    let ressource: Ressource = {
      name: values[0],
      smallDescription: values[1],
      isFree: values[2]=="true"
    }
    this.ressourcesService.insert(ressource).subscribe(ressource => {
      this.findAll();
      
      Swal.fire({
        toast: true,
        position: 'top-end',
        icon: 'success',
        title: 'Your ressource has been added',
        showConfirmButton: false,
        timer: 1500
      })
    });
  }

  update(id){
    this.ressourcesService.findOne(id).subscribe(ressource => {
      Swal.mixin({
        input: 'text',
        confirmButtonText: 'Next &rarr;',
        showCancelButton: true,
        progressSteps: ['1', '2', '3']
      }).queue([
        {
          title: 'Name',
          input: 'text',
          inputValue: ressource.name
        },
        {
          title: 'Small description',
          input: 'text',
          inputValue: ressource.smallDescription
        },
        {
          title: 'Is it free ?',
          input: 'select',
          inputOptions: {
            true: 'Yes',
            false: 'No'
          },
          inputValue: (ressource.isFree ? "true" : "false"),
        }
      ]).then((result) => {
        if (result.value) {
          this.validateUpdate(id, result.value);
        }
      })
    });
  }

  validateUpdate(id, values){
    this.isLoaded = false;
    let ressource: Ressource = {
      name: values[0],
      smallDescription: values[1],
      isFree: values[2]=="true"
    }
    this.ressourcesService.update(id, ressource).subscribe(ressource => {
      this.findAll();
      
      Swal.fire({
        toast: true,
        position: 'top-end',
        icon: 'success',
        title: 'Your ressource has been updated',
        showConfirmButton: false,
        timer: 1500
      })
    });
  }


  delete(id){
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })
    
    swalWithBootstrapButtons.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel!',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        this.validateDelete(id);

        swalWithBootstrapButtons.fire({
          toast: true,
          position: 'top-end',
          title: 'Deleted!',
          text: 'Your ressource has been deleted.',
          icon: 'success',
          showConfirmButton: false,
          timer: 1500
        })
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire({
          toast: true,
          position: 'top-end',
          title: 'Cancelled',
          text: 'Your ressource is safe :)',
          icon: 'error',
          showConfirmButton: false,
          timer: 1500
        })
      }
    })
  }

  validateDelete(id){
    this.isLoaded = false;
    this.ressourcesService.delete(id).subscribe(res => {
      this.findAll();
    });
  }
}