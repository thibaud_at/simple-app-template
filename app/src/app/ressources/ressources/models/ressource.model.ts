export interface Ressource {
  id?: number;
  name: string;
  smallDescription: string;
  isFree: boolean;
  createdAt?: Date;
  updatedAt?: Date;
  version?: number;
}