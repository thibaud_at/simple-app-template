import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { RessourcesModule } from './ressources/ressources.module';

require('dotenv').config({ path: '../.env' });

async function bootstrap() {

  const app = await NestFactory.create(AppModule, { cors : true, logger: ['log', 'error', 'warn', 'debug', 'verbose'] } );
  
  const options = new DocumentBuilder()
    .setTitle('Ressources API')
    .setDescription('The ressources API')
    .setVersion('1.0')
    .setBasePath('api/v1')
    .build();

  const swaggerDocument = SwaggerModule.createDocument(app, options, {
    include: [RessourcesModule],
  });

  SwaggerModule.setup('api/v1/docs', app, swaggerDocument);

  await app.listen(3000);
}
bootstrap();
