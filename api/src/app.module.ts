import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RessourcesModule } from './ressources/ressources.module';

require('dotenv').config()

@Module({
  imports: [
    TypeOrmModule.forRoot({
      "type": "postgres",
      "host": process.env.DB_HOST,
      "port": parseInt(process.env.DB_PORT),
      "username": process.env.DB_USERNAME,
      "password": process.env.DB_PASSWORD,
      "database": process.env.DB_DATABASE,
      "entities": ["dist/**/*.entity{.ts,.js}"],
      "synchronize": true
    }),
    RessourcesModule
  ],
  controllers: [AppController],
  providers: [],
})
export class AppModule {}
