import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Ressource } from './models/ressource.entity';
import { RessourcesService } from './services/ressources.service';
import { RessourcesController } from './controllers/ressources.controller';

@Module({
  imports: [TypeOrmModule.forFeature([Ressource])],
  providers: [RessourcesService],
  controllers: [RessourcesController],
})
export class RessourcesModule {}
