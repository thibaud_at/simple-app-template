import {
  Entity,
  CreateDateColumn,
  UpdateDateColumn,
  VersionColumn,
  PrimaryGeneratedColumn,
  Column
} from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';

@Entity({ name: 'ressource' })
export class Ressource {
  @ApiProperty()
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @Column()
  name: string;

  @ApiProperty()
  @Column({ name: 'small_description' })
  smallDescription: string;

  @ApiProperty()
  @Column({ name: 'is_free' })
  isFree: boolean;

  @ApiProperty()
  @CreateDateColumn({ name: 'created_at', type: 'timestamp' })
  createdAt: Date;

  @ApiProperty()
  @UpdateDateColumn({ name: 'updated_at', type: 'timestamp' })
  updatedAt: Date;

  @ApiProperty()
  @VersionColumn()
  version: number;
}
