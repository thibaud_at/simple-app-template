import { Controller } from '@nestjs/common';
import { Crud, CrudController } from '@nestjsx/crud';
import { Ressource } from '../models/ressource.entity';
import { RessourcesService } from '../services/ressources.service';

@Crud({
  model: {
    type: Ressource,
  }
})
@Controller('api/v1/ressources')
export class RessourcesController implements CrudController<Ressource>{
  constructor(public service: RessourcesService) {}
}