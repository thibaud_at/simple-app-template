import { Injectable } from '@nestjs/common';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Ressource } from '../models/ressource.entity';

@Injectable()
export class RessourcesService extends TypeOrmCrudService<Ressource> {
  constructor(@InjectRepository(Ressource) repo) {
    super(repo);
  }
}
